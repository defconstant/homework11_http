import java.io.*;
import java.net.*;

public class HttpRequest {
	private static final String URI = "lena.ru";
	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String ACCEPT_ENCODING = "gzip,deflate,sdch";
	private static final String PROTOCOL = "http";
	private static final int PORT = 80;
	private static final String PAGE = "/afisha.html";

	public static void main(String[] args) throws Exception {
		URL url = new URL(PROTOCOL, URI, PORT, PAGE);
		HttpURLConnection connect = (HttpURLConnection) url.openConnection();
		connect.setRequestMethod("POST");
		connect.setRequestProperty("User-Agent", USER_AGENT);
		connect.setRequestProperty("Accept-Encoding", ACCEPT_ENCODING);
		connect.setDoOutput(true);
		OutputStreamWriter out = new
			OutputStreamWriter(connect.getOutputStream());
		out.write("test");
		out.close();
		int responseCode = connect.getResponseCode();
		System.out.println("POST Response Code :: " + responseCode);
		if (responseCode == HttpURLConnection.HTTP_OK) {
			BufferedReader in = new BufferedReader(new InputStreamReader(
			connect.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) 
				System.out.println(inputLine);
			in.close();
		}
	}
}
